# 用户信息

------

##### 获取用户信息

|参数 | 类型 |  说明 |
| ---- | ------------------ | ---- |
| encryptedData | String |  包括敏感数据在内的完整用户信息的加密数据|
| iv |  String |  加密算法的初始向量|
| code |  String | 用户的code  |

```js
const user = await wxa.getUserInfo( encryptedData, iv, code );
```

##### 绑定微信用户为小程序体验者

|参数 | 类型 |  说明 |
| ---- | ------------------ | ---- |
| wechatid |  String | 测试微信号  |

```js
const bindTester = await wxa.bindTester( wechatid );
```

##### 解除绑定小程序的体验者

|参数 | 类型 |  说明 |
| ---- | ------------------ | ---- |
| wechatid |  String | 测试微信号  |

```js
const unbindTester = await wxa.unbindTester( wechatid );
```